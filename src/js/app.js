import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready
  
  const ul = document.querySelector("ul");
  let url = "https://pokeapi.co/api/v2/pokemon?limit=10";


  fetch(url)
    .then(response => response.json())
    .then(data => {
      for (let index = 0; index < data.results.length; index++) {
        let liItem = document.createElement("li");
        liItem.innerText = data.results[index].name;
        ul.appendChild(liItem);
      }
    });
});
